module.exports = {
    'name'  : 'portfolio12',
    'camel' : 'Portfolio12',
    'slug'  : 'portfolio-12',
    'dob'   : 'Portfolio_12_1440',
    'desc'  : 'Rows of images for a logo wall.',
}