<!-- Start portfolio12 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : TBD -->
@endif
<section class="{{ $block->classes }}" is="fir-portfolio-12" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}" data-observe-resizes>
  <div class="portfolio-12 {{ $theme }} {{ $text_align }} {{ $flip }}">
      @if($title)
      <h2>{{ $title }}</h2>
      @endif
      @foreach($images as $image)
        <img src="{{ $image['image']['url'] }}" alt="{{ $image['image']['alt'] }}">
      @endforeach
  </div>
</section>
<!-- End portfolio12 -->
