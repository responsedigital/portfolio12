class Portfolio12 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }

    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse(this.querySelector('script[type="application/json"]').text())
        } catch (e) {}
        return data
    }

    resolveElements () {
    }

    connectedCallback () {
        this.initPortfolio12()
    }

    initPortfolio12 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: portfolio12")
    }
}

window.customElements.define('fir-portfolio-12', Portfolio12, { extends: 'section' })
